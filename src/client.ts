import { Map } from "./common-types";
import { ViewState } from "./core";

export class ForestResponse {
    public static empty = () => new ForestResponse({});

    public readonly path: string;
    public readonly shell: ViewState;
    public readonly views: Map<ViewState>;

    constructor({path = '', shell = {}, views = {}}) {
        this.path = path;
        this.shell = new ViewState(shell);
        const views1: {[index: string]:any} = views || {};
        this.views = Object.keys(views1)
            .map((key) => { return { key, data: new ViewState(views1[key]) } })
            .reduce((a, b) => { return {...a, [b.key]: b.data } }, {});
    }
}

export interface IForestClient {
    navigate: (template: string) => Promise<ForestResponse>,
    invokeCommand: (instanceId: string, command: string, arg?: any) => Promise<ForestResponse>
}

class NoopForestClient implements IForestClient {
    navigate: (template: string) => Promise<ForestResponse> = () => Promise.reject();
    invokeCommand: (instanceId: string, command: string, arg?: any) => Promise<ForestResponse> = () => Promise.reject();
}

export const NoopClient = new NoopForestClient();