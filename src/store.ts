import { ViewState, RegionMap, EMPTY_HIERARCHY } from "./core";
import { ForestResponse } from "./client";

type Mapping<P, Q> = { (state: P) : Q }
type ViewStateSelector = (key: string) => Mapping<ForestResponse, ViewState>;
type RootHierarchySelector = Mapping<ForestResponse, RegionMap>;

export const viewStateSelector: ViewStateSelector = (key: string) => (appState: ForestResponse) => appState.views[key];

export const rootHierarchySelector: RootHierarchySelector = (state: ForestResponse) => state.shell.regions || EMPTY_HIERARCHY;